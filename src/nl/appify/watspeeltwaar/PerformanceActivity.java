package nl.appify.watspeeltwaar;

import java.util.ArrayList;

import nl.appify.watspeeltwaar.model.DataModel;
import nl.appify.watspeeltwaar.model.ListItemPerformance;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.example.watspeeltwaar.R;

/**
 * 
 * @author Groep 1 (Wessel)
 * @version 1.0
 * 
 *          Dit is de ArtistActivity die word aangeroepen als deze word
 *          aangeroepen vanuit een andere Activity
 */
public class PerformanceActivity extends SherlockActivity implements OnChildClickListener {

	private ArrayList<ListItemPerformance> itemArrayList;
	public ActionBar actionBar;
	private DataModel dataModel;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_performance);

		// DataModel inladen
		dataModel = new DataModel(this);

		Bundle extras = getIntent().getExtras();

		// Eigenschappen van de actionbar
		actionBar = getSupportActionBar();
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle("Alle artiesten");

		int type = extras.getInt("type");
		int item_id = extras.getInt("id");
		int event_id = extras.getInt("event_id");

		Log.d("extras", type+" "+item_id+" "+event_id);

		itemArrayList = new ArrayList<ListItemPerformance>();

		// Toevoegen van tekst aan de knoppen
		if(type == 0)
		{
			itemArrayList = dataModel.getPerformancesByArtist(event_id, item_id);

			TextView eventTitle = (TextView)this.findViewById(R.id.performanceTitle);
			eventTitle.setText("Optredens ("+dataModel.getArtistName(item_id) + ")");

			TextView performanceInfo = (TextView)this.findViewById(R.id.performanceInfo);
			performanceInfo.setText(dataModel.getArtistDescription(item_id));
		}
		else
		{
			itemArrayList = dataModel.getPerformancesByLocation(event_id, item_id);

			TextView eventTitle = (TextView)this.findViewById(R.id.performanceTitle);
			eventTitle.setText("Optredens ("+dataModel.getLocationName(item_id) + ")");

			TextView performanceInfo = (TextView)this.findViewById(R.id.performanceInfo);
			performanceInfo.setText(dataModel.getLocationDescription(item_id));
		}

		// ListView vullen
		ExpandableListView listView = (ExpandableListView) this.findViewById(R.id.listViewPerformance);

		PerformanceListAdapter arrayAdapter = new PerformanceListAdapter(this,itemArrayList);

		// dan de adapter aan de lijst koppelen
		listView.setAdapter(arrayAdapter);
		listView.setOnChildClickListener(this);
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		com.actionbarsherlock.view.MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.main, (com.actionbarsherlock.view.Menu) menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	/**
	 * Roept de menu handler aan met de naam van de knop die is ingedrukt
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean handled = MenuHandler.onOptionsItemSelected(item,this); 
		if (!handled) {
			handled = super.onOptionsItemSelected(item);
		}
		return handled;
	}

	public boolean onChildClick(ExpandableListView parent, View v,
			int groupPosition, int childPosition, long id) {
		
			Log.d("id", "bert");
		
		return false;
	}
	
	public DataModel getDataModel()
	{
		return this.dataModel;
	}

}
