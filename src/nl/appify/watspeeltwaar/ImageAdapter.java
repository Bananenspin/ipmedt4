package nl.appify.watspeeltwaar;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.example.watspeeltwaar.R;

/**
 * 
 * @author Groep 1 (Dennis)
 * @version 1.0
 * 
 *          Dit is de adapter voor slider op de homepagina. Hij vult de slider
 *          met de plaatjes.
 * 
 */

public class ImageAdapter extends BaseAdapter {
	private Context mContext;

	public ImageAdapter(Context c) {
		mContext = c;
	}

	public int getCount() {
		return mImageIds.length;
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	// View voor de Slider
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView i = new ImageView(mContext);
		i.setImageResource(mImageIds[position]);
		i.setScaleType(ImageView.ScaleType.FIT_XY);
		return i;
	}

	// Invoegen afbeeldingen slider
	private Integer[] mImageIds = { R.drawable.gallery_photo_1,
			R.drawable.gallery_photo_2, R.drawable.gallery_photo_3,
			R.drawable.gallery_photo_4, };
}