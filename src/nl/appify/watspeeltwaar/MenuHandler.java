package nl.appify.watspeeltwaar;

import java.util.Calendar;

import nl.appify.watspeeltwaar.model.ApiModel;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.example.watspeeltwaar.R;

/**
 * 
 * @author Groep 1 (Dennis)
 * @version 1.0
 * 
 *          Dit is de MenuHandler die in elke activity word aangeroepen en alle
 *          menu knopjes afhandeld qua clicks.
 */
public class MenuHandler {
	public static boolean onOptionsItemSelected(MenuItem item,
			final SherlockActivity context) {
		// Aanmaken van het optiemenu in de menubalk
		Log.d("menu", item.getItemId() + "");

		switch (item.getItemId()) {
		case R.id.action_help:
			Log.d("debug", "help");
			return true;
		case android.R.id.home:
			context.finish();
			return true;
		case R.id.action_settings:
			Log.d("debug", "Settings");
			return true;
		case R.id.action_reminders:
			Log.d("debug", "reminders");
			Calendar cal = Calendar.getInstance();
			Intent intent = new Intent(Intent.ACTION_EDIT);
			intent.setType("vnd.android.cursor.item/event");
			intent.putExtra("beginTime", cal.getTimeInMillis());
			intent.putExtra("allDay", false);
			intent.putExtra("rrule", "FREQ=DAILY");
			intent.putExtra("endTime", cal.getTimeInMillis() + 60 * 60 * 1000);
			intent.putExtra("title", "A Test Event from android app");
			context.startActivity(intent);
			return true;
		case R.id.settings_button:
			new Handler().postDelayed(new Runnable() {
				public void run() {
					context.openOptionsMenu();
				}
			}, 50);
			return true;
		case R.id.refresh_button:
			ApiModel api = new ApiModel(context);
			api.execute("");
			return true;
		default:
			return false; // allow default processing
		}
	}
}
