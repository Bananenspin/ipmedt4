package nl.appify.watspeeltwaar;

import nl.appify.watspeeltwaar.model.DataModel;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;

/**
 * 
 * @author Groep 1 (Wessel)
 * @version 1.0
 * 
 *          Dit is de listener voor optredens die afhandeld om een route
 *          beschrijving te tonen op je scherm.
 *          Jelle wielinga heeft het voorbereidende werk voor deze klasse gedaan.
 */
public class PerformanceClickListener implements OnClickListener {

	private int performance_id;
	private Context context;

	public PerformanceClickListener(int performance_id, Context context) {
		this.performance_id = performance_id;
		this.context = context;
	}

	public void onClick(View v) {
		DataModel dataModel = new DataModel(context);

		String address = dataModel.getPerformanceAddress(performance_id);

		Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
				Uri.parse("http://maps.google.com/maps?daddr=" + address));
		context.startActivity(intent);
	}
}
