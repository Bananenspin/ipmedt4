package nl.appify.watspeeltwaar;

import java.util.ArrayList;

import com.example.watspeeltwaar.R;

import nl.appify.watspeeltwaar.model.ListItemGroup;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * 
 * @author Groep 1 (Dennis)
 * @version 1.0
 * 
 *          Dit is de adapter voor de lijst met alle groepen van een evenement.
 *          De adapter zorgt ervoor dat de listview gevuld word met de juiste
 *          informatie. Wanneer je op een locatie drukt ga je naar alle
 *          optredens op die locatie.
 */
public class GroupListAdapter extends BaseAdapter {

	// Dit is de lijst met alle items
	// De lijst wordt gevuld in een andere klasse, we gebruiken de lijst hier
	// alleen om uit te lezen
	private ArrayList<ListItemGroup> itemArrayList;
	static TextView mDotsText[];
	private int[] colors = new int[] { R.drawable.list_background_dark,
			R.drawable.list_background_light };

	public GroupListAdapter(ArrayList<ListItemGroup> itemArrayList) {
		this.itemArrayList = itemArrayList;
	}

	public int getCount() {
		// Geef het aantal objecten terug dat in de itemArrayList staan
		return itemArrayList.size();
	}

	public Object getItem(int position) {
		// Geef het item terug dat op de gevraagde positie staat in de
		// itemArrayList
		// In dit geval is dat het object dat op de positie "position" staat in
		// de arraylist
		return itemArrayList.get(position);
	}

	public long getItemId(int position) {
		// geef de index terug van het element die op de gevraagde positie staat
		// in de itemArrayList
		// In dit geval is de positie "position" gelijk aan de index in de
		// arraylist
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		TextView naam, count;

		if (convertView == null) {
			// if it's not recycled, initialize some
			// bij een niet-bestaand listview item wordt een nieuw item gemaakt
			// gebruik als Context object voor de inflater parent.getContext()
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());
			convertView = inflater.inflate(R.layout.group_item, parent, false);

			// Aanroepen van de tekst in de Listview
			naam = (TextView) convertView.findViewById(R.id.naam);
			count = (TextView) convertView.findViewById(R.id.count);

		} else {
			naam = (TextView) convertView.findViewById(R.id.naam);
			count = (TextView) convertView.findViewById(R.id.count);
			convertView.setId(position);
		}

		// Haal het item op uit de lijst dat op de meegegeven positie getoond
		// moet worden
		ListItemGroup item = (ListItemGroup) getItem(position);

		// Vul het item met de juiste data die zojuist is opgehaald
		naam.setText(item.getNaam());
		count.setText(item.getCount());

		// Achtergrondkleur voor de listview
		int colorPos = position % colors.length;
		convertView.setBackgroundResource(colors[colorPos]);

		return convertView;
	}
}
