package nl.appify.watspeeltwaar;

import java.util.ArrayList;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.example.watspeeltwaar.R;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

import nl.appify.watspeeltwaar.model.*;

/**
 * 
 * @author Groep 1 (Remco, Jelle, Wessel en Dennis)
 * @version 1.0
 * 
 *          Dit is de MainActivity die word aangeroepen als de applicatie word
 *          opgestart.
 */
public class MainActivity extends SherlockActivity implements
		OnItemClickListener {

	private ArrayList<ListItemHome> itemArrayList;
	public ActionBar actionBar;
	static ImageView mDots[];
	private int mDotsCount;
	private DataModel dataModel;
	private LinearLayout sliderLayout;
	private SharedPreferences prefs;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		prefs = this.getSharedPreferences(null, 0);

		if (Integer.parseInt("" + prefs.getLong("last_update", 0)) == 0) {
			ApiModel api = new ApiModel(this);
			api.execute("");
		} else {
			// Eigenschappen van de actionbar
			setContentView(R.layout.activity_home);
			actionBar = getSupportActionBar();
			actionBar.setHomeButtonEnabled(false);
			actionBar.setDisplayHomeAsUpEnabled(false);
			actionBar.setDisplayShowTitleEnabled(true);
			actionBar.setTitle("Wat Speelt Waar");

			dataModel = new DataModel(this);

			ListView listView = (ListView) this.findViewById(R.id.listViewHome);

			itemArrayList = dataModel.getEvents();

			HomeListAdapter arrayAdapter = new HomeListAdapter(itemArrayList,
					this);

			// dan de adapter aan de lijst koppelen
			listView.setAdapter(arrayAdapter);
			listView.setOnItemClickListener(this);
			
			
			/**
			 * Slider gemaakt door Remco
			 */
			// Aanroepen van de Slider
			CustomGallery g = (CustomGallery) findViewById(R.id.gallery);
			g.setSpacing(2);

			// Slider koppelen aan de ImageAdapter
			g.setAdapter(new ImageAdapter(this));

			// De dots aanroepen op de slider
			sliderLayout = (LinearLayout) findViewById(R.id.image_count);
			mDotsCount = g.getAdapter().getCount();

			// Aanmaken dots op de Slider
			mDots = new ImageView[mDotsCount];

			for (int i = 0; i < mDotsCount; i++) {
				mDots[i] = new ImageView(this);
				sliderLayout.addView(mDots[i]);
			}
			g.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() { // Status
																					// van
																					// de
																					// dots
																					// bepalen
																					// dmv
																					// afbeeldingen
				public void onItemSelected(AdapterView adapterView, View view,
						int pos, long l) {
					for (int i = 0; i < mDotsCount; i++) {
						MainActivity.mDots[i]
								.setImageResource(R.drawable.slider_inactive);
					}
					MainActivity.mDots[pos]
							.setImageResource(R.drawable.slider_active);
				}

				public void onNothingSelected(AdapterView adapterView) {
				}
			});
		}
		
		/**
		 * Einde code Remco
		 */
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		com.actionbarsherlock.view.MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.main, (com.actionbarsherlock.view.Menu) menu);
		return super.onCreateOptionsMenu(menu);
	}

	/**
	 * Roept de menu handler aan met de naam van de knop die is ingedrukt
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean handled = MenuHandler.onOptionsItemSelected(item, this);
		if (!handled) {
			handled = super.onOptionsItemSelected(item);
		}
		return handled;
	}

	/**
	 * Opent een nieuwe Activity aan de hand van welk item is aangeklikt in de
	 * lijst
	 */
	public void onItemClick(AdapterView<?> av, View v, int position, long id) {
		// Intent intentnaam //Huidige activity nieuwe activity.class
		Intent myIntent = new Intent(MainActivity.this, GroupActivity.class);
		myIntent.putExtra("id", itemArrayList.get(position).getId());
		startActivity(myIntent);
		Log.d("fout", "" + itemArrayList.get(position).getId());
	}

}
