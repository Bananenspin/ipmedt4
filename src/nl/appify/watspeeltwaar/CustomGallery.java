package nl.appify.watspeeltwaar;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.Gallery;

/**
 * 
 * @author Groep 1 (Remco)
 * 
 * Deze class is er om ervoor te zorgen dat bij het sliden van de Gallery, de gebruiker maar 1 afbeelding verder gaat
 * ipv direct door naar de laatste afbeelding. 
 *
 */

public class CustomGallery extends Gallery 
{

    public CustomGallery(Context ctx, AttributeSet attrSet) {
        super(ctx,attrSet);
    }
    
    //Bepaalt wanneer er naar links wordt gescrolled
    private boolean isScrollingLeft(MotionEvent e1, MotionEvent e2){ 
           return e2.getX() > e1.getX(); 
        }

    /**
     * De methode Gallery heeft een standaard onFling methode, deze moet Override worden voordat deze 
     * aangepast kan worden. 
     * De onFling methode regelt de scrollsnelheid waarmee de Gallery per afbeelding wisselt
     */
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY){
      int kEvent;
      if(isScrollingLeft(e1, e2)){ //Check if scrolling left
        kEvent = KeyEvent.KEYCODE_DPAD_LEFT;
      }
      else{ //Otherwise scrolling right
        kEvent = KeyEvent.KEYCODE_DPAD_RIGHT;
      }
      onKeyDown(kEvent, null);
      return true;  
    }
}