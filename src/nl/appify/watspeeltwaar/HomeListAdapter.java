package nl.appify.watspeeltwaar;

import java.util.ArrayList;

import com.example.watspeeltwaar.R;

import nl.appify.watspeeltwaar.image.ImageLoader;
import nl.appify.watspeeltwaar.model.ListItemHome;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 
 * @author Groep 1 (Dennis)
 * @version 1.0
 * 
 *          Dit is de adapter voor de lijst op de eerste pagina. De adapter
 *          zorgt ervoor dat de listview gevuld word met de juiste informatie.
 *          Wanneer er op een evement word gedrukt gaat hij naar de volgende
 *          pagina met bijbehorende informatie van dat evenement
 * 
 */

public class HomeListAdapter extends BaseAdapter {

	private ArrayList<ListItemHome> itemArrayList;
	static TextView mDotsText[];
	private int[] colors = new int[] { R.drawable.list_background_dark,
			R.drawable.list_background_light };
	public ImageLoader imageLoader;
	private MainActivity activity;

	/**
	 * 
	 * @param itemArrayList
	 * @param a
	 * 
	 *            De constructor deze krijgt de MainActivity mee en een
	 *            arrayList met ListItemHomeclasses mee
	 */
	public HomeListAdapter(ArrayList<ListItemHome> itemArrayList, MainActivity a) {
		this.itemArrayList = itemArrayList;
		this.activity = a;

		// Nieuwe klasse van ImageLoader maken voor het laden van de plaatjes
		imageLoader = new ImageLoader(activity.getApplicationContext());
	}

	public int getCount() {
		// Geef het aantal objecten terug dat in de itemArrayList staan
		return itemArrayList.size();
	}

	public Object getItem(int position) {
		// Geef het item terug dat op de gevraagde positie staat in de
		// itemArrayList
		// In dit geval is dat het object dat op de positie "position" staat in
		// de arraylist
		return itemArrayList.get(position);
	}

	public long getItemId(int position) {
		// geef de index terug van het element die op de gevraagde positie staat
		// in de itemArrayList
		// In dit geval is de positie "position" gelijk aan de index in de
		// arraylist
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		TextView itemNaam, itemLocatie, itemDatum;
		ImageView itemPlaatje;

		if (convertView == null) {
			// if it's not recycled, initialize some
			// bij een niet-bestaand listview item wordt een nieuw item gemaakt
			// gebruik als Context object voor de inflater parent.getContext()
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());
			convertView = inflater.inflate(R.layout.home_item, parent, false);

			// Aanroepen van de tekst in de Listview
			itemNaam = (TextView) convertView.findViewById(R.id.itemNaam);
			itemLocatie = (TextView) convertView.findViewById(R.id.itemLocatie);
			itemDatum = (TextView) convertView.findViewById(R.id.itemDatum);
			itemPlaatje = (ImageView) convertView.findViewById(R.id.imageView1);

		} else {
			itemNaam = (TextView) convertView.findViewById(R.id.itemNaam);
			itemLocatie = (TextView) convertView.findViewById(R.id.itemLocatie);
			itemDatum = (TextView) convertView.findViewById(R.id.itemDatum);
			itemPlaatje = (ImageView) convertView.findViewById(R.id.imageView1);
			convertView.setId(position);
		}

		// Haal het item op uit de lijst dat op de meegegeven positie getoond
		// moet worden
		ListItemHome item = (ListItemHome) getItem(position);

		// Vul het item met de juiste data die zojuist is opgehaald
		itemNaam.setText(item.getNaam());
		itemLocatie.setText(item.getLocatie());
		itemDatum.setText(item.getDatum());

		// Achtergrondkleur voor de listview
		int colorPos = position % colors.length;
		convertView.setBackgroundResource(colors[colorPos]);

		imageLoader.DisplayImage(item.getPlaatje(), itemPlaatje);

		return convertView;

	}
}
