package nl.appify.watspeeltwaar;

import java.util.ArrayList;

import nl.appify.watspeeltwaar.model.DataModel;
import nl.appify.watspeeltwaar.model.ListItemLocation;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.example.watspeeltwaar.R;

/**
 * 
 * @author Groep 1 (Wessel)
 * @version 1.0
 * 
 *          Dit is de ArtistActivity die word aangeroepen als deze word
 *          aangeroepen vanuit een andere Activity
 */
public class LocationActivity extends SherlockActivity implements OnItemClickListener {
	
	public ActionBar actionBar;
	private DataModel dataModel;
	private ArrayList<ListItemLocation> itemArrayList;
	private Bundle extras;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_artist);

		// DataModel inladen
		dataModel = new DataModel(this);

		extras = getIntent().getExtras();

		// Eigenschappen van de actionbar
		actionBar = getSupportActionBar();
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle("Alle artiesten");

		Log.d("fout", "Artiesten geopend met event id: " + extras.getInt("id"));

		TextView eventTitle = (TextView)this.findViewById(R.id.EventTitle);

		eventTitle.setText(dataModel.getEventTitle(extras.getInt("id")));
		
		
		// ListView vullen
		ListView listView = (ListView) this.findViewById(R.id.listViewArtist);

		itemArrayList = dataModel.getLocations(extras.getInt("id"));
		
		LocationListAdapter arrayAdapter = new LocationListAdapter(itemArrayList);
		
		// dan de adapter aan de lijst koppelen
		listView.setAdapter(arrayAdapter);
		listView.setOnItemClickListener(this);
		
		Log.d("list", itemArrayList.toString());
		

	}

	public boolean onCreateOptionsMenu(Menu menu) {
		com.actionbarsherlock.view.MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.main, (com.actionbarsherlock.view.Menu) menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	/**
	 * Roept de menu handler aan met de naam van de knop die is ingedrukt
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
	    boolean handled = MenuHandler.onOptionsItemSelected(item,this); 
	    if (!handled) {
	        handled = super.onOptionsItemSelected(item);
	    }
	    return handled;
	}

	/**
	 * Opent een nieuwe Activity aan de hand van welk item is aangeklikt in de
	 * lijst
	 */
	public void onItemClick(AdapterView<?> av, View v, int position, long id) {
		Log.d("fout", "" + itemArrayList.get(position).getId());
		Intent myIntent = new Intent(LocationActivity.this, PerformanceActivity.class);
		myIntent.putExtra("event_id", extras.getInt("id"));
		myIntent.putExtra("id", itemArrayList.get(position).getId());
		myIntent.putExtra("type", 1);
		startActivity(myIntent);
	}
}
