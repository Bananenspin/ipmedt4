package nl.appify.watspeeltwaar;

import java.util.ArrayList;

import com.actionbarsherlock.app.SherlockActivity;
import com.example.watspeeltwaar.R;

import nl.appify.watspeeltwaar.model.ListItemPerformance;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 
 * @author Groep 1 (Wessel)
 * @version 1.0
 * 
 *          Dit is de adapter voor de optredens lijst. De adapter zorgt ervoor
 *          dat de listview gevuld word met de juiste informatie. Wanneer er op
 *          een optreden word geklikt toont hij ook een extra rij met knoppen.
 *          Deze klase zorgt er ook voor dat een notifcatie ingesteld kan
 *          worden.
 *          
 *          Het voorbereidende/onderzoek werk voor de expandable listview is gedaan door Jelle Wielinga
 * 
 */

public class PerformanceListAdapter extends BaseExpandableListAdapter implements
		OnClickListener {

	private ArrayList<ListItemPerformance> itemArrayList;
	static TextView mDotsText[];
	private int[] colors = new int[] { R.drawable.list_background_dark,
			R.drawable.list_background_light };
	private PackageManager m_pkgMgr;
	private SherlockActivity context;
	private Button reminder_button, route_button;

	/**
	 * 
	 * @param context
	 * @param itemArrayList
	 * 
	 *            De constructor deze krijgt de context en een arrayList met
	 *            ListItemPerformance classes mee
	 */
	public PerformanceListAdapter(SherlockActivity context,
			ArrayList<ListItemPerformance> itemArrayList) {
		this.itemArrayList = itemArrayList;
		this.context = context;
		this.m_pkgMgr = context.getPackageManager();
	}

	public Object getChild(int groupPos, int childPos) {
		return new Object();
	}

	public long getChildId(int groupPos, int childPos) {
		return childPos;
	}

	public int getChildrenCount(int groupPos) {
		return 1;
	}

	public View getChildView(int groupPos, int childPos, boolean isLastChild,
			View convertView, ViewGroup parent) {
		if (convertView == null) {
			// if it's not recycled, initialize some
			// bij een niet-bestaand listview item wordt een nieuw item gemaakt
			// gebruik als Context object voor de inflater parent.getContext()
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());
			convertView = inflater.inflate(R.layout.performance_item_child,
					parent, false);

			// Aanroepen van de tekst in de Listview
			reminder_button = (Button) convertView.findViewById(R.id.reminder);
			route_button = (Button) convertView.findViewById(R.id.route);

			ListItemPerformance item = (ListItemPerformance) this
					.getGroup(groupPos);

			reminder_button.setOnClickListener(this);
			reminder_button.setTag(item.getId());

			route_button.setOnClickListener(new PerformanceClickListener(item
					.getId(), context));

		} else {
			convertView.setId(childPos);
		}
		// Haal het item op uit de lijst dat op de meegegeven positie getoond
		// moet worden
		ListItemPerformance item = (ListItemPerformance) getGroup(childPos);

		// Vul het item met de juiste data die zojuist is opgehaald

		return convertView;
	}

	public Object getGroup(int position) {
		// Geef het item terug dat op de gevraagde positie staat in de
		// itemArrayList
		// In dit geval is dat het object dat op de positie "position" staat in
		// de arraylist
		return itemArrayList.get(position);
	}

	public int getGroupCount() {
		// Geef het aantal objecten terug dat in de itemArrayList staan
		return itemArrayList.size();
	}

	public long getGroupId(int position) {
		// geef de index terug van het element die op de gevraagde positie staat
		// in de itemArrayList
		// In dit geval is de positie "position" gelijk aan de index in de
		// arraylist
		return position;
	}

	public View getGroupView(int position, boolean isExpanded,
			View convertView, ViewGroup parent) {

		TextView itemNaam, itemLocatie, itemDatum;

		if (convertView == null) {
			// if it's not recycled, initialize some
			// bij een niet-bestaand listview item wordt een nieuw item gemaakt
			// gebruik als Context object voor de inflater parent.getContext()
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());
			convertView = inflater.inflate(R.layout.performance_item, parent,
					false);

			// Aanroepen van de tekst in de Listview
			itemNaam = (TextView) convertView.findViewById(R.id.itemNaam);
			itemLocatie = (TextView) convertView.findViewById(R.id.itemLocatie);
			itemDatum = (TextView) convertView.findViewById(R.id.itemDatum);
		} else {
			itemNaam = (TextView) convertView.findViewById(R.id.itemNaam);
			itemLocatie = (TextView) convertView.findViewById(R.id.itemLocatie);
			itemDatum = (TextView) convertView.findViewById(R.id.itemDatum);
			convertView.setId(position);
		}
		// Haal het item op uit de lijst dat op de meegegeven positie getoond
		// moet worden
		ListItemPerformance item = (ListItemPerformance) getGroup(position);

		// Vul het item met de juiste data die zojuist is opgehaald
		itemNaam.setText(item.getNaam());
		itemLocatie.setText(item.getLocatie());
		itemDatum.setText(item.getDatum());

		// Achtergrondkleur voor de listview
		int colorPos = position % colors.length;
		convertView.setBackgroundResource(colors[colorPos]);

		return convertView;
	}

	public boolean hasStableIds() {
		return false;
	}

	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	/**
	 * Opvangen van de klik. Wanneer het de reminder button is roep de juiste
	 * functie aan.
	 */
	public void onClick(View v) {
		if (reminder_button.getId() == ((Button) v).getId()) {
			Log.d("fout", "reminder");

			int id = (Integer) v.getTag();

			Log.d("boos", id + "");

			// display in short period of time
			Toast.makeText(context.getApplicationContext(),
					"De reminder is ingesteld", Toast.LENGTH_SHORT).show();
			createNotification(context, "Hallo dit is een test");

		}
	}

	/* Stelt de notifactie in van het juiste evenement/optreden */

	/**
	 * 
	 * @param context
	 * @param message
	 * 
	 *            Stelt de notifactie in van het juiste evenement/optreden
	 */
	public void createNotification(Context context, String message) {
		int icon = R.drawable.ic_launcher;
		long when = System.currentTimeMillis();
		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(icon, message, when);

		String title = context.getString(R.string.app_name);

		Intent notificationIntent = new Intent(context, MainActivity.class);

		// set intent so it does not start a new activity
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent intent = PendingIntent.getActivity(context, 0,
				notificationIntent, 0);
		notification.setLatestEventInfo(context, title, message, intent);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;

		// Play default notification sound
		notification.defaults |= Notification.DEFAULT_SOUND;

		// Vibrate if vibrate is enabled
		notification.defaults |= Notification.DEFAULT_VIBRATE;
		notificationManager.notify(0, notification);

	}
}
