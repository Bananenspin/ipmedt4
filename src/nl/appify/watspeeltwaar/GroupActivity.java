package nl.appify.watspeeltwaar;

import java.util.ArrayList;

import nl.appify.watspeeltwaar.model.DataModel;
import nl.appify.watspeeltwaar.model.ListItemGroup;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.example.watspeeltwaar.R;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

/**
 * 
 * @author Groep 1 (Dennis)
 * @version 1.0
 * 
 *          Dit is de ArtistActivity die word aangeroepen als deze word
 *          aangeroepen vanuit een andere Activity
 */
public class GroupActivity extends SherlockActivity implements OnItemClickListener {

	private ArrayList<ListItemGroup> itemArrayList;
	public ActionBar actionBar;
	private DataModel dataModel;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_group);

		// DataModel inladen
		dataModel = new DataModel(this);

		Bundle extras = getIntent().getExtras();

		// Eigenschappen van de actionbar
		actionBar = getSupportActionBar();
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(dataModel.getEventTitle(extras.getInt("id")));

		Log.d("fout", "Group is geopend met id: " + extras.getInt("id"));

		TextView eventInfo = (TextView)this.findViewById(R.id.eventInfo);

		eventInfo.setText(dataModel.getEventDescription(extras.getInt("id")));

		ListView listView = (ListView) this.findViewById(R.id.listViewGroup);

		itemArrayList = new ArrayList<ListItemGroup>();
		
		// Toevoegen van de tekst aan de knoppen
		itemArrayList.add(new ListItemGroup("Activiteiten per locatie", extras.getInt("id"), dataModel.getEventLocationCount(extras.getInt("id"))+" locaties"));
		itemArrayList.add(new ListItemGroup("Activiteiten per artiest", extras.getInt("id"),dataModel.getEventArtistCount(extras.getInt("id"))+" artiesten"));

		// we maken tot slot een adapter aan die de data (de arraylist)
		GroupListAdapter arrayAdapter = new GroupListAdapter(itemArrayList);

		// dan de adapter aan de lijst koppelen
		listView.setAdapter(arrayAdapter);

		listView.setOnItemClickListener(this);

	}

	public boolean onCreateOptionsMenu(Menu menu) {
		com.actionbarsherlock.view.MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.main, (com.actionbarsherlock.view.Menu) menu);
		return super.onCreateOptionsMenu(menu);
	}

	/**
	 * Roept de menu handler aan met de naam van de knop die is ingedrukt
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean handled = MenuHandler.onOptionsItemSelected(item,this); 
		if (!handled) {
			handled = super.onOptionsItemSelected(item);
		}
		return handled;
	}
	
	/**
	 * Opent een nieuwe Activity aan de hand van welk item is aangeklikt in de
	 * lijst
	 */
	public void onItemClick(AdapterView<?> av, View v, int position, long id) {

		if(position == 0)
		{
			// Open optredens per locatie
			Intent myIntent = new Intent(GroupActivity.this, LocationActivity.class);
			myIntent.putExtra("id", itemArrayList.get(position).getId());
			startActivity(myIntent);
			
		}
		else if(position == 1)
		{
			// Open optredens per artiest
			Intent myIntent = new Intent(GroupActivity.this, ArtistActivity.class);
			myIntent.putExtra("id", itemArrayList.get(position).getId());
			startActivity(myIntent);
		}
	}
}
