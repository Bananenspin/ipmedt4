package nl.appify.watspeeltwaar.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * 
 * @author Groep 1 (Wessel)
 * @version 1.0
 * 
 *          De database helper, deze maakt de tabellen aan en past eventueel kolomen aan.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

	public DatabaseHelper(Context context) {
		super(context, "wsw.db", null, 2);
		// TODO Auto-generatedconstructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.d("DB","Table aanmaken");
		db.execSQL("CREATE TABLE events (id INTEGER PRIMARY KEY, title TEXT, start_date INTEGER, end_date INTEGER, city TEXT, description TEXT, last_update INTEGER, picture TEXT);");
		db.execSQL("CREATE TABLE locations (id INTEGER PRIMARY KEY, name TEXT, event_id INTEGER, address TEXT, city TEXT, description TEXT, last_update INTEGER);");
		db.execSQL("CREATE TABLE performances (id INTEGER PRIMARY KEY, event_id INTEGER, location_id INTEGER, artist_id INTEGER, start_time INTEGER, end_time INTEGER, last_update INTEGER);");
		db.execSQL("CREATE TABLE artist (id INTEGER PRIMARY KEY, name TEXT, description TEXT, style TEXT, last_update INTEGER);");
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.d("DB","Table upgraden");
		db.execSQL("ALTER TABLE events ADD COLUMN picture TEXT");
	}

}
