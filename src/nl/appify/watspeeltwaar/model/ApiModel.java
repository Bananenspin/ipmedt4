package nl.appify.watspeeltwaar.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.*;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.actionbarsherlock.app.SherlockActivity;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

/**
 * 
 * @author Groep 1 (Wessel)
 * @version 1.0
 * 
 *          Alle api trasacties en SQLite database vullingen gebeuren hier.
 */
public class ApiModel extends AsyncTask {

	private SherlockActivity context;
	private SharedPreferences prefs;
	private ProgressDialog dialog;

	public ApiModel(SherlockActivity context) {
		this.context = context;
		this.dialog = new ProgressDialog(context);

		prefs = context.getSharedPreferences(null, 0);
	}

	/**
	 * Sync zorgt voor een verbinding met de api en haalt de json als een String
	 * op en zet deze om in een JSON array
	 */
	public void sync() {

		Log.d("API", "Api update running");
		StringBuilder builder = new StringBuilder();

		try {
			int last_update = Integer.parseInt(""
					+ prefs.getLong("last_update", 0));
			last_update = 0;
			HttpClient client = new DefaultHttpClient();
			Log.d("url", "http://api.event-leiden.nl/sync/" + last_update);
			HttpGet httpGet = new HttpGet("http://api.event-leiden.nl/sync/"
					+ last_update);
			HttpResponse response = client.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) {
				// Verbinding is gelukt
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();

				BufferedReader reader = new BufferedReader(
						new InputStreamReader(content));

				String line;

				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}

				// Maak een JSONObject van de string
				Log.d("json", builder.toString());
				JSONObject jsonObject = new JSONObject(builder.toString());

				// Sla alle gegevens op in de SqlLite DB
				this.storeData(jsonObject);
			} else {
				Log.e("iets", "Failed to download file");
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Deze functie schrijft de JSON array weg in de SQLite datbase
	 * 
	 * @param data
	 *            - De JSON array die van de API af komt
	 */
	public void storeData(JSONObject data) {
		SQLiteDatabase wsw_db = (new DatabaseHelper(this.context))
				.getWritableDatabase();

		wsw_db.execSQL("DELETE FROM events");
		wsw_db.execSQL("DELETE FROM locations");
		wsw_db.execSQL("DELETE FROM artist");
		wsw_db.execSQL("DELETE FROM performances");

		try {
			JSONArray events = data.getJSONArray("events");
			JSONArray locations = data.getJSONArray("locations");
			JSONArray artist = data.getJSONArray("artist");
			JSONArray performances = data.getJSONArray("performances");

			// Insert alle evenementen in de database
			for (int x = 0; x < events.length(); x++) {
				JSONObject tmp_event = (JSONObject) events.get(x);

				ContentValues values = new ContentValues();
				values.put("id", tmp_event.getString("id"));
				values.put("title", tmp_event.getString("title"));
				values.put("start_date", tmp_event.getString("start_date"));
				values.put("end_date", tmp_event.getString("end_date"));
				values.put("city", tmp_event.getString("city"));
				values.put("description", tmp_event.getString("description"));
				values.put("last_update", tmp_event.getString("last_update"));
				values.put("picture", tmp_event.getString("filename"));

				wsw_db.replace("events", null, values);

			}

			// Insert alle locaties in de database
			for (int x = 0; x < locations.length(); x++) {
				JSONObject tmp_locations = (JSONObject) locations.get(x);

				ContentValues values = new ContentValues();
				values.put("id", tmp_locations.getString("id"));
				values.put("name", tmp_locations.getString("name"));
				values.put("event_id", tmp_locations.getString("event_id"));
				values.put("address", tmp_locations.getString("address"));
				values.put("city", tmp_locations.getString("city"));
				values.put("description",
						tmp_locations.getString("description"));
				values.put("last_update",
						tmp_locations.getString("last_update"));

				wsw_db.replace("locations", null, values);

			}

			// Insert alle artiesten in de database
			for (int x = 0; x < artist.length(); x++) {

				JSONObject tmp_artist = (JSONObject) artist.get(x);

				ContentValues values = new ContentValues();
				values.put("id", tmp_artist.getString("id"));
				values.put("name", tmp_artist.getString("name"));
				values.put("description", tmp_artist.getString("description"));
				values.put("style", tmp_artist.getString("style"));
				values.put("last_update", tmp_artist.getString("last_update"));

				wsw_db.replace("artist", null, values);

			}

			// Insert alle performances in de database
			for (int x = 0; x < performances.length(); x++) {
				JSONObject tmp_performances = (JSONObject) performances.get(x);

				ContentValues values = new ContentValues();
				values.put("id", tmp_performances.getString("id"));
				values.put("event_id", tmp_performances.getString("event_id"));
				values.put("location_id",
						tmp_performances.getString("location_id"));
				values.put("artist_id", tmp_performances.getString("artist_id"));
				values.put("start_time",
						tmp_performances.getString("start_time"));
				values.put("end_time", tmp_performances.getString("end_time"));
				values.put("last_update",
						tmp_performances.getString("last_update"));

				wsw_db.replace("performances", null, values);

			}

			wsw_db.close();

		} catch (JSONException e) {
			e.printStackTrace();
		}

		Editor editor = prefs.edit();

		editor.putLong("last_update", System.currentTimeMillis() / 1000L);
		editor.commit();
	}

	@Override
	protected void onPreExecute() {
		this.dialog.setTitle("Data bijwerken");
		this.dialog.setMessage("De data wordt nu bijgewerkt...");
		this.dialog.show();
	}

	@Override
	protected Object doInBackground(Object... params) {

		this.sync();

		this.dialog.dismiss();

		context.finish();
		context.startActivity(context.getIntent());

		return null;
	}
}
