package nl.appify.watspeeltwaar.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * 
 * @author Groep 1 (Wessel)
 * @version 1.0
 * 
 *          Deze klasse word gebruikt om data uit de database op te halen en
 *          deze weer terug te geven aan de activity's die hem aanroepen
 */
public class DataModel {

	private Context context;
	private SQLiteDatabase wsw_db;
	private ArrayList<ListItemHome> homeitemArrayList;
	private ArrayList<ListItemArtist> artistitemArrayList;
	private ArrayList<ListItemLocation> locationitemArrayList;
	private ArrayList<ListItemPerformance> performanceitemArrayList;

	public DataModel(Context context) {
		this.context = context;
	}

	public ArrayList<ListItemHome> getEvents() {
		wsw_db = (new DatabaseHelper(this.context)).getWritableDatabase();
		Cursor c = wsw_db.rawQuery(
				"SELECT * FROM events ORDER BY start_date ASC", null);

		int count = c.getCount();

		Log.d("db", "Count: " + count);

		homeitemArrayList = new ArrayList<ListItemHome>();

		while (c.moveToNext()) {

			// Timestamp omzetten naar datum

			long longFrom = Long.valueOf(c.getString(2)) * 1000;// its need to
																// be in
																// milisecond
			Date dateFrom = new java.util.Date(longFrom);
			String stringFrom = new SimpleDateFormat("dd-MM").format(dateFrom);

			long longTill = Long.valueOf(c.getString(3)) * 1000;
			Date dateTill = new java.util.Date(longTill);
			String stringTill = new SimpleDateFormat("dd-MM").format(dateTill);

			String complete = new String();

			complete = stringFrom;
			if (stringTill != "") {
				complete = complete + " t/m " + stringTill;
			}

			String plaatje = c.getString(c.getColumnIndex("picture"));

			homeitemArrayList.add(new ListItemHome(c.getString(1), Integer
					.parseInt(c.getString(0)), c.getString(4), complete,
					plaatje));
		}

		c.close();
		wsw_db.close();
		return homeitemArrayList;
	}

	public ArrayList<ListItemArtist> getArtists(int event_id) {
		wsw_db = (new DatabaseHelper(this.context)).getWritableDatabase();
		Cursor c = wsw_db
				.rawQuery(
						"SELECT a.name, a.id FROM performances p JOIN artist a ON p.artist_id = a.id WHERE event_id = '"
								+ event_id + "' GROUP BY a.id", null);

		int count = c.getCount();

		Log.d("db", "Count: " + count);

		artistitemArrayList = new ArrayList<ListItemArtist>();

		while (c.moveToNext()) {

			artistitemArrayList.add(new ListItemArtist(c.getString(0), Integer
					.parseInt(c.getString(1)), getEventArtistIdCount(event_id,
					Integer.parseInt(c.getString(1))) + " optreden(s)"));
		}

		c.close();
		wsw_db.close();
		return artistitemArrayList;
	}

	public ArrayList<ListItemLocation> getLocations(int event_id) {
		wsw_db = (new DatabaseHelper(this.context)).getWritableDatabase();
		Cursor c = wsw_db.rawQuery(
				"SELECT l.name, l.id FROM locations l WHERE event_id = '"
						+ event_id + "' ", null);

		int count = c.getCount();

		Log.d("db", "Count: " + count);

		locationitemArrayList = new ArrayList<ListItemLocation>();

		while (c.moveToNext()) {

			locationitemArrayList.add(new ListItemLocation(c.getString(0),
					Integer.parseInt(c.getString(1)), getEventLocationIdCount(
							event_id, Integer.parseInt(c.getString(1)))
							+ " optreden(s)"));
		}

		c.close();
		wsw_db.close();
		return locationitemArrayList;
	}

	public ArrayList<ListItemPerformance> getPerformancesByArtist(int event_id,
			int artist_id) {
		wsw_db = (new DatabaseHelper(this.context)).getWritableDatabase();

		Cursor c = wsw_db.rawQuery(
				"SELECT p.id, a.name, p.start_time, p.end_time, l.name FROM performances p "
						+ "JOIN artist a ON p.artist_id = a.id "
						+ "JOIN locations l ON p.location_id = l.id "
						+ "WHERE p.artist_id = '" + artist_id + "'", null);

		int count = c.getCount();

		Log.d("db", "Count: " + count);

		performanceitemArrayList = new ArrayList<ListItemPerformance>();

		while (c.moveToNext()) {
			// Alle velden ophalen
			int id = Integer.parseInt(c.getString(0));
			String artist_name = c.getString(1);
			String start_time = c.getString(2);
			String end_time = c.getString(3);
			String location_name = c.getString(4);

			// Timestamp omzetten naar datum

			long longFrom = Long.valueOf(start_time) * 1000;// its need to be in
															// milisecond
			Date dateFrom = new java.util.Date(longFrom);
			String stringFrom = new SimpleDateFormat("dd-MM HH:mm")
					.format(dateFrom);

			long longTill = Long.valueOf(end_time) * 1000;
			Date dateTill = new java.util.Date(longTill);
			String stringTill = new SimpleDateFormat("HH:mm").format(dateTill);

			String complete = new String();

			complete = stringFrom;
			if (stringTill != "") {
				complete = complete + " - " + stringTill;
			}

			performanceitemArrayList.add(new ListItemPerformance(artist_name,
					id, complete, location_name));
		}

		return performanceitemArrayList;
	}

	public ArrayList<ListItemPerformance> getPerformancesByLocation(
			int event_id, int location_id) {
		wsw_db = (new DatabaseHelper(this.context)).getWritableDatabase();

		Cursor c = wsw_db.rawQuery(
				"SELECT p.id, a.name, p.start_time, p.end_time, l.name FROM performances p "
						+ "JOIN artist a ON p.artist_id = a.id "
						+ "JOIN locations l ON p.location_id = l.id "
						+ "WHERE p.location_id = '" + location_id + "'", null);

		int count = c.getCount();

		Log.d("db", "Count: " + count);

		performanceitemArrayList = new ArrayList<ListItemPerformance>();

		while (c.moveToNext()) {
			// Alle velden ophalen
			int id = Integer.parseInt(c.getString(0));
			String artist_name = c.getString(1);
			String start_time = c.getString(2);
			String end_time = c.getString(3);
			String location_name = c.getString(4);

			// Timestamp omzetten naar datum

			long longFrom = Long.valueOf(start_time) * 1000;// its need to be in
															// milisecond
			Date dateFrom = new java.util.Date(longFrom);
			String stringFrom = new SimpleDateFormat("dd-MM HH:mm")
					.format(dateFrom);

			long longTill = Long.valueOf(end_time) * 1000;
			Date dateTill = new java.util.Date(longTill);
			String stringTill = new SimpleDateFormat("HH:mm").format(dateTill);

			String complete = new String();

			complete = stringFrom;
			if (stringTill != "") {
				complete = complete + " - " + stringTill;
			}

			performanceitemArrayList.add(new ListItemPerformance(artist_name,
					id, complete, location_name));
		}

		return performanceitemArrayList;
	}

	public String getArtistName(int artist_id) {
		wsw_db = (new DatabaseHelper(this.context)).getWritableDatabase();
		Cursor c = wsw_db.rawQuery("SELECT name FROM artist WHERE id = '"
				+ artist_id + "'", null);

		c.moveToFirst();
		String ret = c.getString(0);

		c.close();
		wsw_db.close();

		return ret;
	}

	public String getArtistDescription(int artist_id) {
		wsw_db = (new DatabaseHelper(this.context)).getWritableDatabase();
		Cursor c = wsw_db
				.rawQuery("SELECT description FROM artist WHERE id = '"
						+ artist_id + "'", null);

		c.moveToFirst();
		String ret = c.getString(0);

		c.close();
		wsw_db.close();

		return ret;
	}

	public String getLocationName(int location_id) {
		wsw_db = (new DatabaseHelper(this.context)).getWritableDatabase();
		Cursor c = wsw_db.rawQuery("SELECT name FROM locations WHERE id = '"
				+ location_id + "'", null);

		c.moveToFirst();
		String ret = c.getString(0);

		c.close();
		wsw_db.close();

		return ret;
	}

	public String getLocationDescription(int location_id) {
		wsw_db = (new DatabaseHelper(this.context)).getWritableDatabase();
		Cursor c = wsw_db.rawQuery(
				"SELECT description FROM locations WHERE id = '" + location_id
						+ "'", null);

		c.moveToFirst();
		String ret = c.getString(0);

		c.close();
		wsw_db.close();

		return ret;
	}

	public String getEventDescription(int event_id) {
		wsw_db = (new DatabaseHelper(this.context)).getWritableDatabase();
		Cursor c = wsw_db.rawQuery(
				"SELECT description FROM events WHERE id = '" + event_id + "'",
				null);

		c.moveToFirst();
		String ret = c.getString(0);

		c.close();
		wsw_db.close();

		return ret;
	}

	public String getPerformanceAddress(int performance_id) {
		wsw_db = (new DatabaseHelper(this.context)).getWritableDatabase();
		Cursor c = wsw_db.rawQuery(
				"SELECT l.address, l.city FROM performances p "
						+ "JOIN locations l ON p.location_id = l.id "
						+ "WHERE p.id = '" + performance_id + "'", null);

		Log.d("sql", "SELECT l.address, l.city FROM performances p "
				+ "JOIN locations l ON p.location_id = l.id "
				+ "WHERE p.id = '" + performance_id + "'");

		c.moveToFirst();
		String ret = c.getString(0);
		ret = ret + ", " + c.getString(1);

		c.close();
		wsw_db.close();

		return ret;
	}

	public String getEventTitle(int event_id) {
		wsw_db = (new DatabaseHelper(this.context)).getWritableDatabase();
		Cursor c = wsw_db.rawQuery("SELECT title FROM events WHERE id = '"
				+ event_id + "'", null);
		c.moveToFirst();
		String ret = c.getString(0);
		c.close();
		wsw_db.close();
		return ret;
	}

	public int getEventArtistCount(int event_id) {
		wsw_db = (new DatabaseHelper(this.context)).getWritableDatabase();
		Cursor c = wsw_db.rawQuery(
				"SELECT id FROM performances WHERE event_id = '" + event_id
						+ "' GROUP BY artist_id", null);
		int ret = c.getCount();
		c.close();
		wsw_db.close();
		return ret;
	}

	public int getEventArtistIdCount(int event_id, int artist_id) {
		wsw_db = (new DatabaseHelper(this.context)).getWritableDatabase();
		Cursor c = wsw_db.rawQuery(
				"SELECT id FROM performances WHERE event_id = '" + event_id
						+ "' AND artist_id = '" + artist_id + "'", null);
		int ret = c.getCount();
		c.close();
		wsw_db.close();
		return ret;
	}

	public int getEventLocationCount(int event_id) {
		wsw_db = (new DatabaseHelper(this.context)).getWritableDatabase();
		Cursor c = wsw_db.rawQuery(
				"SELECT id FROM locations WHERE event_id = '" + event_id + "'",
				null);
		int ret = c.getCount();
		c.close();
		wsw_db.close();
		return ret;
	}

	public int getEventLocationIdCount(int event_id, int location_id) {
		wsw_db = (new DatabaseHelper(this.context)).getWritableDatabase();
		Cursor c = wsw_db.rawQuery(
				"SELECT id FROM performances WHERE event_id = '" + event_id
						+ "' AND location_id = '" + location_id + "'", null);
		int ret = c.getCount();
		c.close();
		wsw_db.close();
		return ret;
	}
}
