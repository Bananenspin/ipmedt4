package nl.appify.watspeeltwaar.model;

/**
 * 
 * @author Groep 1 (Wessel)
 * @version 1.0
 * 
 *          Deze class slaat informatie op over de verschillende optredens die
 *          een locatie heeft en kan ze ook weer terug geven
 */

public class ListItemPerformance {

	private String naam;
	private int id;
	private String datum;
	private String locatie;

	public ListItemPerformance(String naam, int id, String datum, String locatie) {
		this.naam = naam;
		this.id = id;
		this.datum = datum;
		this.locatie = locatie;
	}

	public String getNaam()
	{
		return this.naam;
	}

	public int getId()
	{
		return this.id;
	}

	public String getDatum()
	{
		return this.datum;
	}
	
	public String getLocatie()
	{
		return this.locatie;
	}


}
