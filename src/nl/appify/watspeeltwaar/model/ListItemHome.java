package nl.appify.watspeeltwaar.model;

/**
 * 
 * @author Groep 1 (Dennis)
 * @version 1.0
 * 
 *          Deze class slaat informatie op over de verschillende evenementen die
 *          plaats vinden en kan ze ook weer terug geven
 */
public class ListItemHome {
	
	private String naam;
	private int id;
	private String locatie;
	private String datum;
	private String plaatje;
	
	public ListItemHome(String naam, int id, String locatie, String datum, String plaatje) {
		this.naam = naam;
		this.id = id;
		this.locatie = locatie;
		this.datum = datum;
		this.plaatje = plaatje;
	}

	public String getNaam() {
		return naam;
	}

	public void setNaam(String naam) {
		this.naam = naam;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLocatie() {
		return locatie;
	}

	public void setLocatie(String locatie) {
		this.locatie = locatie;
	}

	public String getDatum() {
		return datum;
	}

	public void setDatum(String datum) {
		this.datum = datum;;
	}

	public String getPlaatje() {
		return "http://admin.event-leiden.nl/images/events/" + plaatje;
	}

	public void setPlaatje(String plaatje) {
		this.plaatje = plaatje;
	}
	
	

	
}
