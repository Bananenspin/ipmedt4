package nl.appify.watspeeltwaar.model;

/**
 * 
 * @author Groep 1 (Wessel)
 * @version 1.0
 * 
 *          Deze class slaat informatie op over de verschillende locaties die
 *          een evenement heeft en kan ze ook weer terug geven
 */

public class ListItemLocation {

	private String naam;
	private int id;
	private String count;

	public ListItemLocation(String naam, int id, String count) {
		this.naam = naam;
		this.id = id;
		this.count = count;
	}

	public String getNaam()
	{
		return this.naam;
	}

	public int getId()
	{
		return this.id;
	}

	public String getCount()
	{
		return this.count;
	}


}
